package de.rayba82.healtcheck;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HealthCheckerTest {


    @Test
    public void testSuccessfulConnection() {
        final HealthChecker healthChecker = new HealthChecker("https://www.google.de");
        assertTrue("Cannot reach Google, something must be wrong", healthChecker.isHealthy());
    }

    @Test
    public void testUnsuccessfulConnection() {
        final HealthChecker healthChecker = new HealthChecker("https://www.myFakeUri.de");
        assertFalse("reached fake URL, something is wrong", healthChecker.isHealthy());
    }

}