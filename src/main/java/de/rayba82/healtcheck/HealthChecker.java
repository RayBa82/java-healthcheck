package de.rayba82.healtcheck;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;

public class HealthChecker {

    private final static Logger LOG = LogManager.getLogger(HealthChecker.class);
    private static final String INFO_MSG = "Got {0} response code {1} from {2}";

    private String urlTocheck;

    HealthChecker(String url) {
        this.urlTocheck = url;
    }

    boolean isHealthy() {
        HttpURLConnection connection = null;
        try {
            final URL url = new URL(urlTocheck);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            final int code = connection.getResponseCode();
            final boolean healthy = code >= 200 && code < 400;
            final String status = healthy ? "healthy" : "unhealthy";
            LOG.info(MessageFormat.format(INFO_MSG, status, code, urlTocheck));
            return healthy;
        } catch (IOException e) {
            LOG.error("Error trying to reach " + urlTocheck, e);
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
        return false;
    }

    public static void main(String[] args) {
        if(args == null || args.length == 0) {
            System.out.println("No check URL provided");
            System.exit(1);
        }
        final HealthChecker checker = new HealthChecker(args[0]);
        if(checker.isHealthy()){
            System.exit(0);
        }
        else {
            System.exit(1);
        }
    }

}